<?php

namespace Tests;

use App\Models\User\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    protected function registerUser(string $name, string $referralCode = null) {

        $this->deleteUser($name);

        //User's data
        $data = [
            'email' => $name . '@test.com',
            'name' => $name,
            'password' => 'secret1234',
        ];
        if ($referralCode) {
            $data['referral_code'] = $referralCode;
        }

        $response = $this->json('POST',route('api.register'),$data);
        return $response;
    }



    protected function getUser(string $name) : ?User {
        return User::where('email', $name . '@test.com')->first();
    }

    protected function deleteUser(string $name) {
        //User's data
        User::where('email', $name . '@test.com')->delete();
    }
}
