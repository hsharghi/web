<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<div class="container">
    <div class="row justify-content-center">
        <div class="form-group row">
            <h1>Login successful</h1>
        </div>
        <div class="form-group row">
            <div class="col-md-8 offset-md-2">
                <a class="btn btn-primary" href="{{ env('APP_SCHEME')."?token=$token" }}">
                    {{ __('Back to App') }}
                </a>
            </div>
        </div>
    </div>
</div>
