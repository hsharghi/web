@extends('layouts.app')

@section('content')
        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>
<body>
<div class="flex-center position-ref full-height">
    <h1>Upload</h1>
    <div id="app">
        {{-- <input name="token" v-model="url_token"> --}}
        <uploader-component api-url="{{$apiUrl}}" upload-url="{{$uploadUrl}}"></uploader-component>
    </div>
</div>
</body>
</html>

@endsection
