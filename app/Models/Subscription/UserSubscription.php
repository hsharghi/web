<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $table = 'user_subscriptions';
    protected $primaryKey = 'user_id';
    protected $guarded = ['user_id'];

    public $timestamps = false;


    public function plan()
    {
        return $this->belongsTo(SubscriptionType::class, 'subscription_plan_id');
    }

}
