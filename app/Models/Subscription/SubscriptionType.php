<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Model;

class SubscriptionType extends Model
{
    protected $table = 'subscription_types';

    public function plan()
    {
        return $this->hasMany(SubscriptionPlan::class, 'subscription_type_id');
    }

}
