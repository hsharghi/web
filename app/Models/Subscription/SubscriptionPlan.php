<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    public function type()
    {
        return $this->belongsTo(SubscriptionType::class, 'subscription_type_id');
    }
}
