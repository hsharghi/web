<?php
/**
 * Created by PhpStorm.
 * User: hadi
 * Date: 2019-03-31
 * Time: 13:35
 */

namespace App\Models\User\Auth;

//use App\Models\User\AccessToken; // AccessToken from step 1
use League\OAuth2\Server\Entities\ClientEntityInterface;
use Laravel\Passport\Bridge\AccessTokenRepository as PassportAccessTokenRepository;

class AccessTokenRepository extends PassportAccessTokenRepository
{
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
    {
        return new AccessToken($userIdentifier, $scopes); // AccessToken from step 1
    }
}