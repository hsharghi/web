<?php

namespace App\Models\User;

use App\Models\Model\UserModels;
use App\Models\Subscription\SubscriptionType;
use App\Models\Subscription\UserSubscription;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function subscription()
    {
        return $this->hasOne(UserSubscription::class)->with('plan');
    }

    public function subscriptionPlan()
    {
        return $this->hasOneThrough(SubscriptionType::class, UserSubscription::class,
                                    'user_id', 'id',
                                    'id', 'subscription_plan_id');
    }

    public function getReferralCodeAttribute()
    {
        return $this->info->referral_code;
    }


    public function uploadToken()
    {
        return $this->hasOne(UploadToken::class, 'user_id', 'id');
    }

    public function models()
    {
        return $this->hasMany(UserModels::class);
    }
}
