<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UploadToken extends Model
{
    protected $table = 'upload_tokens';
    protected $primaryKey = 'user_id';
    protected $guarded = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
