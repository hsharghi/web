<?php

namespace App\Models\Model;

use App\Models\Category\Category;
use App\Models\Category\ModelCategories;
use App\Models\Tag\ModelTags;
use App\Models\Tag\Tag;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class UserModels extends Model
{
    protected $table = 'user_models';
    protected $guarded = ['id', 'user_id'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function categories()
    {
        return $this->hasManyThrough(Category::class, ModelCategories::class);
    }

    public function tags()
    {
        return $this->hasManyThrough(Tag::class, ModelTags::class);
    }

    public function status()
    {
        return $this->hasOne(ConvertHistory::class, 'model_id', 'id');
    }
}
