<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Model;

class ConvertHistory extends Model
{
    protected $table = 'convert_history';
    protected $primaryKey = 'model_id';
    protected $guarded = [];

    public function model()
    {
        return $this->belongsTo(UserModels::class, 'id', 'model_id');
    }

}
