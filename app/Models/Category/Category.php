<?php

namespace App\Models\Category;

use App\Models\Model\UserModels;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function models()
    {
        return $this->hasManyThrough(UserModels::class, ModelCategories::class);
    }
}
