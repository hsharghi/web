<?php

namespace App\Models\Tag;

use App\Models\Model\UserModels;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    public function models()
    {
        return $this->hasManyThrough(UserModels::class, ModelTags::class);
    }
}
