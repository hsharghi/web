<?php

namespace App\Providers;

use App\Events\UserRegistered;
use App\Listeners\Registration\AssignSubscriptionPlan;
use App\Listeners\Registration\FireRegisteredEvent;
use App\Listeners\Registration\GenerateReferralCode;
use App\Listeners\Registration\ReferrerPayment;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserRegistered::class => [
            FireRegisteredEvent::class,
            GenerateReferralCode::class,
            ReferrerPayment::class,
            AssignSubscriptionPlan::class,
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
