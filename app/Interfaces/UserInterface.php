<?php
/**
 * Created by PhpStorm.
 * User: hadi
 * Date: 2019-04-01
 * Time: 12:04
 */

namespace App\Interfaces;

use App\Models\User\User;

interface UserInterface {
    /**
     * Return referrer user if exists
     *
     * @return User|null
     */
    public function getReferrer() : ?User;

    /**
     * Return a user with given referral code
     *
     * @param string $code
     * @return User|null
     */
    public function getReferrerByCode(string $code) : ?User;


    /**
     *
     * Generates unique URL with embedded data
     *
     * @return string|null
     */
    public function generateUploadToken() : string;

}