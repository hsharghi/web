<?php
/**
 * Created by PhpStorm.
 * User: hadi
 * Date: 2019-04-01
 * Time: 12:04
 */

namespace App\Repositories;

use App\Interfaces\UserInterface;
use App\Models\Subscription\SubscriptionType;
use App\Models\User\UploadToken;
use App\Models\User\User;
use App\Models\Subscription\UserSubscription;
use Carbon\Carbon;

class UserRepository implements UserInterface
{

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /** @inheritdoc */
    public function getReferrer(): ?User
    {
        if ($id = $this->user->info->referrer_id) {
            return User::find($id);
        }

        return null;
    }

    /** @inheritdoc */
    public function getReferrerByCode(string $code): ?User
    {
        $user = User::query()->whereHas('info', function($query) use ($code) {
            $query->where('referral_code', $code);
        })->first();

        return $user;
    }


    /**
     * Set a subscription plan for user, update existing if previous entry found
     *
     * @param SubscriptionType $plan
     * @param \DateInterval|null $duration
     *
     */
    public function assignSubscriptionPlan(SubscriptionType $plan, \DateInterval $duration = null)
    {
        $subscriptionData = [
            'subscription_plan_id' => $plan->id,
            'start_date' => Carbon::now(),
            'end_date' => $duration == null ? null : Carbon::now()->add($duration)
        ];

        if ($currentSubscription = $this->user->subscription) {
            $currentSubscription->update($subscriptionData);
            return;
        }

        $this->user->subscription()->save(new UserSubscription($subscriptionData));

    }


    /** @inheritdoc */
    public function generateUploadToken(): string
    {

        $subscriptionPlan = $this->user->subscriptionPlan;
        $data = [
            'userId' => $this->user->id,
            'maxUploadSize' => $subscriptionPlan->max_upload_size,
            'totalStorage' => $subscriptionPlan->storage * 1024,    // convert gb to mb
        ];

        $token = base64_encode(serialize($data));

        return $token;


        $token = md5($this->user->id . time());

        if ($userToken = $this->user->uploadToken) {
            $userToken->token = $token;
        } else {
            $userToken = new UploadToken([
                                             'token' => $token
                                         ]);
        }

        $this->user->uploadToken()->save($userToken);

        return $token;

    }

}