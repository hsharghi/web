<?php

namespace App\Jobs;

use App\Models\Model\ConvertHistory;
use App\Models\Model\UserModels;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ConverterListener implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dirPath = \Storage::disk('upload')->path('');
        if (!\File::isDirectory($dirPath)) {
            return;
        }

        foreach (\File::directories($dirPath) as $userDir) {
            if (!\File::isDirectory($userDir)) {
                continue;
            }

            $rawModelsDir = $userDir . '/raw';
            if (!\File::isDirectory($rawModelsDir)) {
                continue;
            }
            foreach (\File::allFiles($rawModelsDir) as $newModel) {

                $count = \DB::table('user_models')
                    ->where('filename', $newModel->getBasename())
                    ->count();
                if ($count > 0) {
                    continue;
                }

                if ($user = User::find(basename($userDir))) {
                    $model = new UserModels([
                                                'filename' => $newModel->getBasename(),
                                                'title' => 'Model-' . Carbon::now()->format('Y-m-d_His')
                                            ]);
                    $userModel = $user->models()->save($model);

                    $convert = new ConvertHistory([]);
                    $userModel->status()->save($convert);

                }

            }
        }

    }
}
