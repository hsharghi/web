<?php

namespace App\Http\Middleware;

use Closure;

class UploadLargeFile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws PostSizeExceededException
     */
    public function handle($request, Closure $next)
    {
        if (isset($_SERVER["CONTENT_LENGTH"])
            && ($_SERVER["CONTENT_LENGTH"] > ((int)ini_get('post_max_size') * 1024 * 1024))) {

            $currentPostMaxSize = (int)ini_get('post_max_size');

            $ratio = 500 / $currentPostMaxSize;
            $request['upload_max_filesize'] = $ratio * (int)ini_get('upload_max_filesize');
            $request['post_max_size'] = $ratio * (int)ini_get('post_max_size');
            $request['max_input_time'] = $ratio * (int)ini_get('max_input_time');
            $request['max_execution_time'] = $ratio * (int)ini_get('max_execution_time');

            ini_set('upload_max_filesize', $request['upload_max_filesize']);
            ini_set('post_max_size', $request['post_max_size']);
            ini_set('max_input_time', $request['max_input_time']);
            ini_set('max_execution_time', $request['max_execution_time']);

        }

        return $next($request);
    }
}


class PostSizeExceededException extends \Exception
{
    public $validator = null;
}