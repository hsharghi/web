<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver, $origin)
    {
        $redirectUrl = config('services.'.$driver.'.redirect')."?origin={$origin}";

        return Socialite::driver($driver)->stateless()->redirectUrl($redirectUrl)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($driver, Request $request)
    {
        $origin = $request->origin ?? 'web';

        $oAuthUser = Socialite::driver($driver)->stateless()->user();

        if (! $user = User::where('email', $oAuthUser->getEmail())->first()) {
            $user = User::create([
                                     'name' => $oAuthUser->getName(),
                                     'email' => $oAuthUser->getEmail(),
                                     'email_verified_at' => Carbon::now(),
                                     'password' => $oAuthUser->token
                                 ]);
            event(new UserRegistered($user));

        }

        if ($origin == 'web') {
            Auth::login($user, true);
            return redirect('/home');
        }

        $token = $user->createToken('model-bazar')->accessToken;

        return view('auth.oauth-app-callback', compact('token'));
    }


}
