<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\UpdateUserInfoRequest;
use App\Http\Resources\UserProfileResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Return user info.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = Auth::user();

        return $this->response(UserProfileResource::make($user));
    }


    /**
     * Update user info.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserInfoRequest $request)
    {
        $user = Auth::user();

        $user->info()->update($request->validated());

        return $this->response(UserProfileResource::make($user));
    }

    public function avatar(Request $request) {

        $user = Auth::user();

        try {
            if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                $oldAvatar = $user->info->avatar;
                $path = \Storage::disk('avatar')->put('', $request->avatar);

                $user->info->update(['avatar' => $path]);

                if (\Storage::disk('avatar')->exists($oldAvatar)) {
                    \Storage::disk('avatar')->delete($oldAvatar);
                }

//
//                dispatch(new CreateMultiSizeImages($path, 'avatar'));
//                //resize avatar
//                $fullSize = Image::make(\Storage::disk('avatar')->path($path));
//                $file_ext = pathinfo($path, PATHINFO_EXTENSION);
//                $file_name = pathinfo($path, PATHINFO_FILENAME);
//                $file_dir = pathinfo($path, PATHINFO_DIRNAME);
//                $smPath = \Storage::disk('avatar')->path($file_dir.'/'.$file_name.'_thumb.'.$file_ext);
//                $fullSize->fit(100, null, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save($smPath);

            } else {
                return $this->errorResponse('No avatar file found.');
            }

        } catch (\Exception $exception) {
            return $this->errorResponse('Failed to save avatar');
        }

        return $this->response(['avatar' => \Storage::disk('avatar')->url($path)]);

    }
}
