<?php

namespace App\Http\Controllers\API;

use App\Events\UserRegistered;
use App\Models\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['getUser']);
    }

    public function register(UserRegisterRequest $request)
    {
        $data = $request->validated();
        $user = User::create([
                                 'email' => $data['email'],
                                 'name' => $data['name'],
                                 'password' => \Hash::make($data['password']),
                             ]);

        $referralCode = null;
        if (isset($data['referral_code'])) {
            $referralCode = $data['referral_code'];
        }

        event(new UserRegistered($user, $referralCode));

        $token = $user->createToken('model-bazar')->accessToken;
        return $this->response(['token' => $token]);
    }

    public function login(UserLoginRequest $request)
    {
        $loginData = $request->validated();
        if (Auth::attempt($loginData)) {
            $user = Auth::user();
            $token = $user->createToken('model-bazar')->accessToken;

            return $this->response(['token' => $token]);
        }

        return $this->errorResponse('Authentication failed.', 0, [], 401);

    }

    public function getUser()
    {
        if ($user = Auth::user()) {
            return $this->response(['user' => $user]);
        }

        return $this->errorResponse('Unauthorized', 0, [], 403);
    }
}
