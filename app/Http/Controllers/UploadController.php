<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the upload form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $apiUrl = url(env('STORAGE_URL') . '/v1/');

        $repo = new UserRepository(Auth::user());
        $token = $repo->generateUploadToken();
        $uploadUrl = "upload/{$token}";

        return view('upload', compact('uploadUrl', 'apiUrl'));
    }
}
