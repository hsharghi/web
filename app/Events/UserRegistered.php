<?php

namespace App\Events;

use App\Models\User\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserRegistered implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $code;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param string $code
     *
     * @return void
     */
    public function __construct(User $user, ?string $code = null)
    {
        $this->user = $user;
        $this->code = $code;
    }
}
