<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function(Request $request) {
    return $request->user();
});

Route::group([
                 'prefix' => 'v1',
                 'middleware' => ['throttle:180,1', 'auth:api'],
                 'namespace' => 'API',
             ], function() {
    Route::get('profile', 'ProfileController@get')->name('profile.get');
    Route::patch('profile', 'ProfileController@update')->name('profile.update');
    Route::post('profile/avatar', 'ProfileController@avatar')->name('profile.avatar');


});


Route::get('/', function(Request $request) {
    return 'Web API v. 1.0';
});

Route::group([
                 'prefix' => 'v1',
                 'middleware' => 'throttle:180,1',
                 'namespace' => 'API',
             ], function() {

    Route::post('register', 'AuthController@register')->name('api.register');
    Route::post('login', 'AuthController@login')->name('api.login');
    Route::get('user', 'AuthController@getUser');
});
