<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvertHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convert_history', function (Blueprint $table) {
            $table->bigInteger('model_id')->unsigned()->primary()->index();
            $table->enum('status', ['in_queue', 'preparing', 'converting', 'converted', 'failed']);
            $table->timestamps();

            $table->foreign('model_id')
                ->references('id')->on('user_models')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convert_history');
    }
}
